using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ArrowString : MonoBehaviour
{
    //버그있음 안잡고있어도 true가 뜬다.
    public bool stringIsGrabbed = false;
    private void OnTriggerEnter(Collider other)
    {
        if (other.gameObject.CompareTag("RIGHTHAND"))
        {
            Debug.Log(other.transform.name);
            stringIsGrabbed = true;
        }
        else
        {
            stringIsGrabbed = false;
        }
    }
}
