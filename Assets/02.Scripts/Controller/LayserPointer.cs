using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class LayserPointer : MonoBehaviour
{
    public float rayStartDistance = 1f; // 레이저 포인터 시작 위치
    public float raycastDistance = 20f; // 레이저 포인터 거리
    public LayerMask handLayer;
    
    private LineRenderer layser; // 레이저
    private RaycastHit collidedObject; // 충돌된 객체
    private GameObject currentObject; // 가장 최근에 충돌한 객체를 저장하기 위한 객체
    private RaycastHit hit;

    void Start()
    {
        // 스크립트가 포함된 객체에 라인 렌더러라는 컴포넌트를 넣고있다.
        layser = this.gameObject.AddComponent<LineRenderer>();

        // 레이저 기본 색상 표현
        Material material = new Material(Shader.Find("Standard"));
        material.color = new Color(0, 195, 255, 0.5f);
        layser.material = material;

        // 레이저 꼭지점
        layser.positionCount = 2;

        // 레이저 굵기
        layser.startWidth = 0.005f;
        layser.endWidth = 0.001f;
        
        // 레이저 둥글기?
        layser.numCornerVertices = 90;
        layser.numCapVertices = 90;
    }

    void Update()
    {
        // 업데이트에 넣어 줌으로써, 플레이어가 이동하면 이동을 따라가게 된다.
        // 시작점 위치
        layser.SetPosition(0, transform.position + transform.forward * rayStartDistance);
            layser.SetPosition(1, transform.position + (transform.forward * raycastDistance));

        // 선 만들기
        Debug.DrawRay(transform.position, transform.forward * raycastDistance, Color.green, 0.2f);

        // 충돌 감지 시
        //if (Physics.Raycast(transform.position, transform.forward, out collidedObject, raycastDistance))
        //{
            //Debug.Log(collidedObject.transform.name);
            // 끝점 위치
            //layser.SetPosition(1, collidedObject.point);
        //}
        //else
        //{
            // 레이저에 감지된 것이 없기 때문에 레이저 초기 설정 길이만큼 길게 만든다.
        //}
    }
    private void LateUpdate()
    {
        // 버튼을 누를 경우        
        if (OVRInput.GetDown(OVRInput.Button.PrimaryIndexTrigger, OVRInput.Controller.Touch ) || OVRInput.GetDown(OVRInput.Button.SecondaryIndexTrigger, OVRInput.Controller.Touch))
        {
            layser.material.color = new Color(255, 255, 255, 0.1f); // 흰색
        }
        
        // 버튼을 뗄 경우          
        else if (OVRInput.GetUp(OVRInput.Button.PrimaryIndexTrigger) || OVRInput.GetUp(OVRInput.Button.SecondaryIndexTrigger))
        {
            layser.material.color = new Color(0, 195, 255, 0.1f); // 하늘색
        }
    }
}

