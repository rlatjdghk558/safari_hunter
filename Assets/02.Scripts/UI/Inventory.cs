using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class Inventory : MonoBehaviour
{
    [SerializeField] private GameObject go_InventoryBase;
    [SerializeField] private GameObject go_SlotsParent;
    
    private Slot[] slots;
    public ItemInfoText itemInfo;

    public static bool inventoryActivated = false;

    public int requireSwordBranchCount = 4;
    public int requireSwordRockCount = 6;
    public int requireArrowBranchCount = 7;
    public int requireArrowRockCount = 5;
   
    public Text upBranchText;
    public Text downBranchText;
    public Text upRockText;
    public Text downRockText;



    private void Start()
    {
        slots = go_SlotsParent.GetComponentsInChildren<Slot>();

    }

    private void Update()
    {
        upBranchText.text = itemInfo.branchCount + "/" + requireSwordBranchCount;
        downBranchText.text = itemInfo.branchCount + "/" + requireArrowBranchCount;

        upRockText.text = itemInfo.rockCount + "/" + requireSwordRockCount;
        downRockText.text = itemInfo.rockCount + "/" + requireArrowRockCount;
    }

   
    public void AcquireItem(Item _item, int _count)
    {
        if (Item.ItemType.Equipment == _item.itemType)
        {
            for (int i = 0; i < slots.Length; i++)
            {
                if (slots[i].item != null)
                {
                    if (slots[i].item.itemName == _item.itemName)
                    {
                        slots[i].SetSlotCount(_count);
                        return;
                    }
                }
            }
        }
        for (int i = 0; i < slots.Length; i++)
        {
            if (slots[i].item == null)
            {
                slots[i].AddItem(_item, _count);
                 return;
            }
        }
    }
   
    

}
