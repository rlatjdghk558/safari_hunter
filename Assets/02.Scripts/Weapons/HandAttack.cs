using System;
using Unity.VisualScripting;
using UnityEngine;

public class HandAttack : Weapon
{
    public Larch larch;
    public Pine pine;
    public GameObject branch;
    public GameManager gameManager;

    private float attackedTime;
    [SerializeField] private bool attacked;
    
    private void Awake()
    {
        dmg = 10.2f;
    }

    private void Start()
    {
        attacked = false;
        attackedTime = 0f;
    }

    private void Update()
    {
        if (attacked == true)
        {
            attackedTime += Time.deltaTime;
            if (attackedTime > 0.5f)
            {
                attacked = false;
                attackedTime = 0f;
            }
        }
    }

    private void OnTriggerEnter(Collider other)
    {
        if (other.gameObject.CompareTag("LARCH") && attacked == false)
        {
            other.gameObject.GetComponent<Larch>().larchHp -= 10.2f;
            Debug.Log("larchhp" + other.gameObject.GetComponent<Larch>().larchHp);
            attacked = true;
            
            if (other.gameObject.GetComponent<Larch>().larchHp < 0f)
            {
                Destroy(other.gameObject);
                Instantiate(branch, other.transform.position, other.transform.rotation);
                other.gameObject.tag = "Untagged";
                gameManager.larchCnt--;
            }
        }
        else if (other.gameObject.CompareTag("PINE") && attacked == false)
        {
            other.gameObject.GetComponent<Pine>().pineHp -= 10.2f;
            Debug.Log("pinehp" + other.gameObject.GetComponent<Pine>().pineHp);
            attacked = true;
            
            if (other.gameObject.GetComponent<Pine>().pineHp < 0f)
            {
                Destroy(other.gameObject);
                Instantiate(branch, other.transform.position, other.transform.rotation);
                other.gameObject.tag = "Untagged";
                gameManager.pineCnt--;
            }
        }
    }
}
