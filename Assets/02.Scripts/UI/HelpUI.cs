using System.Collections;
using System.Collections.Generic;
using TMPro;
using UnityEngine;
using UnityEngine.UI;

public class HelpUI : MonoBehaviour
{   
    [SerializeField] private GameObject[] controllers;
    [SerializeField] private GameObject[] helpControllers;
    [SerializeField] private Image helpImage;
    [SerializeField] private TextMeshProUGUI helpText;

    private Color imageColor;
    private Color textColor;    
 
    void Start()
    {   
        // 초기 색 설정
        imageColor = helpImage.color;
        textColor = helpText.color;
    }

    public void SetControllers(bool _isActive)
    {
        for(int i = 0; i < controllers.Length; i++)
        {
            controllers[i].SetActive(_isActive);
        }

        helpControllers[0].SetActive(!_isActive);
        helpControllers[1].SetActive(!_isActive);

        SetColor(_isActive);
    }

    public void SetColor(bool _isActive)
    {
        if(_isActive)
        {   
            helpImage.color = imageColor;
            helpText.color = textColor;
        }
        else
        {
            helpImage.color = textColor;
            helpText.color = imageColor;
        }
    }
}
