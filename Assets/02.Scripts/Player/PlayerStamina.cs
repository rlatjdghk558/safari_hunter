using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PlayerStamina : MonoBehaviour
{
    public float playerStamina ;
    public float playerMaxStamina = 10f;

    private void Awake()
    {
        playerStamina = playerMaxStamina;
    }
    private void Update()
    {
        if( OVRInput.Get(OVRInput.Button.PrimaryIndexTrigger, OVRInput.Controller.Touch) && playerStamina >= 0f)
        {
            UseStamina();
        }
        else if(playerStamina <= 10f)
        {
            GetStamina();
        }
    }
    
    private void UseStamina()//스테미너 사용
    {
        playerStamina -= Time.deltaTime * 3f;
    }
    private void GetStamina()//스테미너 다시 채움
    {
        playerStamina += Time.deltaTime;

    }
}
