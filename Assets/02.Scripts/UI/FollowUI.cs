using System.Collections;
using System.Collections.Generic;
using TreeEditor;
using UnityEngine;
using UnityEngine.UIElements;

public class FollowUI : MonoBehaviour
{
    public GameObject Menu;
    public GameObject center;
    public float offsetRadius = 0.3f;
    [Range(-10f, 10f)] public float distance;
    [Range(-2f, 20f)] public float upDistance;
    
    void Update()
    {
        // 메인메뉴의 회전값에 센터(기준)의 회전값을 넣어요~
        Menu.transform.rotation = center.transform.rotation;
        // 플레이어 센터에 센터(기준)의 위치값 + 전방으로 distance거리만큼을 줘요~
        var MenuPos = center.transform.position + center.transform.forward * distance;
        // 메인메뉴의 위치값을 currentPos에 줘요~
        var currentPos = Menu.transform.position + Menu.transform.up * upDistance;        
        var direction = currentPos - MenuPos;        
        var targetPosition = MenuPos + direction.normalized * offsetRadius;

        Menu.transform.position = Vector3.Lerp(currentPos, targetPosition, 0.5f);
    }   
}
