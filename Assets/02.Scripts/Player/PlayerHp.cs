using System;
using System.Collections;
using System.Collections.Generic;
using System.Runtime.CompilerServices;
using UnityEngine;

public class PlayerHp : MonoBehaviour
{
    public float currentHp;
    public float maxHp = 100f;
    public GameObject damageEffect;
    private float damagedTime = 0f;
   [SerializeField] private bool playerDamaged;

    private void Awake()
    {
        damageEffect.SetActive(false);
        currentHp = maxHp;
    }

  

    private void Update()
    {

        if (playerDamaged == true)
        {
            //damageEffect.SetActive(false);        
            damagedTime += Time.deltaTime;
            if (damagedTime > 0.2f)
            {
                damageEffect.SetActive(false);
            }
            if (damagedTime > 3f)
            {
                playerDamaged = false;
                damagedTime = 0f;
            }
        }
        
    }

    private void OnCollisionEnter(Collision collision)
    {
        if (collision.gameObject.CompareTag("ANIMAL") && playerDamaged == false)
        {
            damageEffect.SetActive(true);
            currentHp -= 20.5f;
            //Debug.Log(currentHp);
            playerDamaged = true;
        }
        else if (collision.gameObject.CompareTag("BOSS") && playerDamaged == false) 
        {
            currentHp -= 50.5f;
            playerDamaged = true;
        }
    }
}