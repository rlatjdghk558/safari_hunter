using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Bear : Animal
{
    [SerializeField] protected float howlHpRate;
    [SerializeField] private bool isHowling = false;
    [SerializeField] private LayerMask bearLayer;
    //[SerializeField] private int randomIdx;
    [SerializeField] protected float howlingRange;
    //[SerializeField] private bool isPatrolling =false;
    [SerializeField] private bool isAttacking = false;
    private bool isHowled = false;
    private float timer;
    [SerializeField] private BoxCollider attackCollider = null;

    public delegate void VoidVoidDelegate();
    private VoidVoidDelegate bearDieDelegate;

    protected override void Awake()
    {
        base.Awake();
        attackRange = 3f;
        traceRange = 10f;
    }
    protected override void Start()
    {
        base.Start();
    }
    private void Update()
    {

        if (!isPatrolling)
        {
            randomPatrolIdx = Random.Range(0, wayPoints.Length);
            isPatrolling = true;
        }
        //if (isAttacking)
        //{
        //    this.transform.LookAt(player.transform.position);
        //}
    }
    protected override IEnumerator CheckState()
    {

        yield return null;
        while (!isDie)
        {

            if (curHp <= 0f)
            {
                State = animalStates.Die;
                isDie = true;
                Debug.Log("die");
            }

            if (!isDie)
            {


                if (GetDistance() >= attackRange && GetDistance() < traceRange && isDamaged == false)//공격범위보다 크고, 트레이스 범위보다 작을때
                {
                    State = animalStates.Trace;
                }
                else if (GetDistance() < attackRange && isDamaged == false)
                {
                    State = animalStates.Attack;
                }
                else if (GetDistance() > traceRange && isDamaged == false)
                {

                    State = animalStates.Patrol;
                    float dist = Vector3.Distance(transform.position, curWayPoints.transform.position);

                    if (dist < 1f)//순찰지점 거의 다오면 다음 인덱스 뽑기
                    {
                        isPatrolling = false;
                        Debug.Log("pickNextIdx");
                        curWayPoints = wayPoints[randomPatrolIdx];
                    }
                }
                else if (isDamaged == true)
                {
                    State = animalStates.Trace;
                    timer += Time.deltaTime;
                    if (timer > damagedTime || GetDistance() < attackRange)
                    {
                        isDamaged = false;
                        timer = 0f;
                    }
                }
            }

            yield return null;

        }
    }
    protected override IEnumerator Action()
    {
        //yield return new WaitForSeconds(0.1f);//CheckState 보다 한템포 늦게 시작하기위한 시간지연
        //행동이 이상하면 이걸 없애고 한프레임 지연으로 바꿀것

        yield return null;
        while (!isDie)
        {
            yield return null;

            switch (State)
            {
                case animalStates.Patrol:
                    //Debug.Log("patrol");
                    agent.isStopped = false;
                    anim.SetBool("isWalk", true);
                    anim.SetBool("isRun", false);
                    agent.destination = curWayPoints.transform.position;//Player위치 받는거 만들기
                    agent.speed = patrolSpeed;
                    break;

                case animalStates.Trace:
                    agent.isStopped = false;
                    //Debug.Log("trace");
                    anim.SetBool("isWalk", false);
                    anim.SetBool("isRun", true);
                    agent.destination = player.transform.position;//Player위치 받는거 만들기
                    agent.speed = traceSpeed;
                    break;

                case animalStates.Attack:
                    anim.SetBool("isWalk", false);
                    anim.SetBool("isRun", false);
                    agent.isStopped = true;
                    //Debug.Log("attack");
                    anim.SetTrigger("isAttack");
                    break;

                case animalStates.Howl:
                    agent.isStopped = true;
                    anim.SetBool("isWalk", false);
                    anim.SetBool("isRun", false);
                    anim.SetTrigger("isHowl");
                    Howling();
                    //동물들 모으는 함수 실행
                    break;


                case animalStates.Die:
                    Debug.Log("diediedie");
                    agent.isStopped = true;
                    anim.SetTrigger("isDie");
                    boxCollider.enabled = false;
                    Destroy(this.gameObject, 5f);
                    break;
            }
        }

    }

    public void StartAttack()
    {
        //isAttacking = true;
        attackCollider.enabled = true;
    }
    public void EndAttack()
    {
        //isAttacking = false;
        attackCollider.enabled = false;
    }
    public void EndHowl()
    {
        isHowled = true;
        isHowling = false;
        isDamaged = true;
        Debug.Log("EndHowl");
    }
    public void Howling()
    {
        Collider[] colls = Physics.OverlapSphere(transform.position, howlingRange, bearLayer);
        for (int i = 0; i < colls.Length; ++i)
        {
            colls[i].GetComponent<Bear>().isDamaged = true;

        }
        Debug.Log(colls.Length);
    }
    private void OnDestroy()
    {
        bearDieDelegate?.Invoke();
    }
    public void SetDelegate(VoidVoidDelegate _bearDieDelegate)
    {
        bearDieDelegate = _bearDieDelegate;
    }
    private void OnCollisionEnter(Collision collision)
    {
        if (collision.gameObject.CompareTag("WEAPON"))
        {
            curHp -= collision.gameObject.GetComponent<Weapon>().Damage();
            isDamaged = true;
        }


    }


}
