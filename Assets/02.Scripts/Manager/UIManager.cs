using System;
using System.Collections;
using System.Collections.Generic;
using Facebook.WitAi.Utilities;
using Unity.VisualScripting;
using UnityEngine;
using UnityEngine.SceneManagement;
using UnityEngine.UI;
using TMPro;
using UnityEngine.AI;

public class UIManager : MonoBehaviour
{
    public GameObject fadeCanvas;
    public Image fadeImage;

    [Header("메뉴 관련")]
    public GameObject mainMenu; // 메인메뉴
    private bool _mainMenu; // 메인메뉴 상태
    private bool isGameClear = false; // 게임클리어 상태
    public GameObject gameOver;
    public GameObject gameClear;

    [Header("플레이어 체력, 스테미너")]
    public Slider hpBar;
    public Slider staminaBar;
    private PlayerStamina playerStamina;
    private PlayerHp playerHp;


    [Header("조합관련")]
    public ItemInfoText itemInfo;
    public Inventory inventory;
    public GameObject inventoryObject;
    public GameObject slotParent;
    private Slot[] slots;
    private bool inventoryOpen = false;
    public TMP_Text swordText;
    public TMP_Text arrowText;
    public Text bearCardText;
    public Text bossBearCardText;

    [Header("아이템 갯수")]
    public static int sword = 0;
    public static int arrow = 0;
    public  int bearCard = 0;
    public  int deerCard = 0;

    public Sword _sword;
    public Slider weaponDurabilitySlider;


    

    public PlayerMovement playerMovement;


    public BoxCollider rightHandBox;
    public BoxCollider leftHandBox;

    [SerializeField] private Bear[] bear;
    [SerializeField] private Deer[] deer;

    [SerializeField] private GameObject bearGroup;
    [SerializeField] private GameObject deerGroup;

    [SerializeField] private GameObject bossPrefab;
    private BearBoss bearBoss;
 
    

    
    
    private void Awake()
    {
        fadeCanvas.SetActive(true);
        //deer = deerGroup.GetComponentsInChildren<Deer>();
        //bear = bearGroup.GetComponentsInChildren<Bear>();
        bearBoss = bossPrefab.GetComponent<BearBoss>();
    }


    void Start()
    {
        deer = deerGroup.GetComponentsInChildren<Deer>();
        bear = bearGroup.GetComponentsInChildren<Bear>();
        for (int i =0; i< deer.Length; ++i)
        {
            deer[i].SetDelegate(PlusDeerCnt);
        }
        for (int i = 0; i < bear.Length; ++i)
        {
            bear[i].SetDelegate(PlusBearCnt);
        }
        bearBoss.SetDelegate(GameClear);
        StartCoroutine(FadeIn());

        // 메인메뉴 초기설정
        _mainMenu = false;
        gameClear.SetActive(false);
        gameOver.SetActive(false);

        //player 관련 설정
        playerHp = GameObject.FindGameObjectWithTag("PLAYER").GetComponent<PlayerHp>();
        playerStamina = GameObject.FindGameObjectWithTag("PLAYER").GetComponent<PlayerStamina>();

        //조합관련
        slots = slotParent.GetComponentsInChildren<Slot>();
        inventoryObject.SetActive(false);

    }

    void Update()
    {
        // 메인메뉴
        if (OVRInput.GetDown(OVRInput.Button.Start, OVRInput.Controller.Touch) && _mainMenu == false)
        {
            // 게임 일시정지되고 (0이면 작동이 안됨)
            Time.timeScale = 0.01f;

            _mainMenu = true; // 게임메뉴상태 on
            mainMenu.SetActive(true); // 게임메뉴 on
            DisableHandCollider();
        }

        else if (OVRInput.GetDown(OVRInput.Button.Start, OVRInput.Controller.Touch) && _mainMenu == true)
        {
            // 게임 일시정지 해제
            Time.timeScale = 1f;

            _mainMenu = false; // 게임메뉴상태 off
            mainMenu.SetActive(false); // 게임메뉴 off
            EnableHandCollider();
        }

        else if (OVRInput.GetDown(OVRInput.Button.Four, OVRInput.Controller.Touch) || Input.GetKey(KeyCode.LeftShift))
        {
            //Debug.Log("스테미너감소");
            DecreaseStaminaBar();
        }

        //StaminaBar 회복
        IncreaseStaminaBar();

        //HpBar 표시
        HpBar();

        //인벤토리 온오프
        TryOpenInventory();

        ShowCurrentArrowCount();//화살 조합
        ShowCurrentSwordCount();//검 조합
        ShowCurrentBearCardCount();//곰카드 
        ShowCurrentBossBearCardCount();//보스곰카드 
        
        // 무기내구도
        ShowWeaponDurability();

        GameOver();

    }

    // 게임메뉴관련
    public void OnClickHelpMenu()
    {
        StartCoroutine(FadeOut("S03_HelpScene 1", 0.9f));
    }
    public void OnClickStartMenu()
    {
        StartCoroutine(FadeOut("S01_StartMenuScene", 0.9f));
    }
    public void OnClickGameOver()
    {
        Application.Quit(); // 게임종료
    }
    private IEnumerator FadeIn()
    {
        yield return new WaitForSeconds(1.5f);
        float fadeCount = 1;
        while (fadeCount > 0.1f)
        {
            fadeCount -= 0.005f;
            yield return new WaitForSeconds(0.01f);
            fadeImage.color = new Color(0, 0, 0, fadeCount);
        }

        if (fadeCount < 0.11f)
            fadeCanvas.SetActive(false);

    }
    private IEnumerator FadeOut(string _sceneName, float _maxfadeCount)
    {
        fadeCanvas.SetActive(true);

        float fadeCount = 0;
        while (fadeCount < _maxfadeCount + 0.2f)
        {
            fadeCount += 0.01f;
            yield return new WaitForSeconds(0.01f);
            fadeImage.color = new Color(0, 0, 0, fadeCount);
        }
        if (fadeCount >= _maxfadeCount) 
            SceneManager.LoadScene(_sceneName);
    }

    public void IncreaseStaminaBar()
    {
        staminaBar.value = playerStamina.playerStamina / playerStamina.playerMaxStamina;
    }

    private void DecreaseStaminaBar()
    {
        staminaBar.value = playerStamina.playerStamina / playerStamina.playerMaxStamina;
    }
    public void HpBar()
    {
        hpBar.value = playerHp.currentHp / playerHp.maxHp;
    }


    public void CombineSword()
    {
        if (itemInfo.branchCount < inventory.requireArrowBranchCount || itemInfo.rockCount < inventory.requireArrowRockCount)
            return;
        if (itemInfo.branchCount < 0 || itemInfo.rockCount < 0)
            return;

       //Debug.Log("123123123123123123123123123");
        itemInfo.branchCount -= inventory.requireSwordBranchCount;
        itemInfo.rockCount -= inventory.requireSwordRockCount;
        sword++;

        for (int i = 0; i < slots.Length; ++i)
        {
            if (slots[i].GetComponent<Slot>().item != null)
            {
                string itemName = slots[i].GetComponent<Slot>().item.name;
                if (itemName == "Branch")
                {
                    slots[i].GetComponent<Slot>().itemCount -= inventory.requireSwordBranchCount;
                    slots[i].GetComponent<Slot>().text_Count.text = slots[i].GetComponent<Slot>().itemCount.ToString();
                }
                else if (itemName == "Rock")
                {
                    slots[i].GetComponent<Slot>().itemCount -= inventory.requireSwordRockCount;
                    slots[i].GetComponent<Slot>().text_Count.text = slots[i].GetComponent<Slot>().itemCount.ToString();
                }
            }
        }
    }

    public void CombineArrow()
    {
        if (itemInfo.branchCount < inventory.requireArrowBranchCount || itemInfo.rockCount < inventory.requireArrowRockCount)
            return;
        if (itemInfo.branchCount < 0 || itemInfo.rockCount < 0)
            return;

        itemInfo.branchCount -= inventory.requireArrowBranchCount;
        itemInfo.rockCount -= inventory.requireArrowRockCount;

        arrow += 10;
        for (int i = 0; i < slots.Length; ++i)
        {
            if (slots[i].GetComponent<Slot>().item != null)
            {
                string itemName = slots[i].GetComponent<Slot>().item.name;
                if (itemName == "Branch")
                {
                    slots[i].GetComponent<Slot>().itemCount -= inventory.requireArrowBranchCount;
                    slots[i].GetComponent<Slot>().text_Count.text = slots[i].GetComponent<Slot>().itemCount.ToString();
                }
                else if (itemName == "Rock")
                {
                    slots[i].GetComponent<Slot>().itemCount -= inventory.requireArrowRockCount;
                    slots[i].GetComponent<Slot>().text_Count.text = slots[i].GetComponent<Slot>().itemCount.ToString();
                }
            }
        }
    }
    

    //인벤토리 온오프
    private void TryOpenInventory()
    {
        if ( OVRInput.GetDown(OVRInput.Button.One, OVRInput.Controller.Touch))
        {
            inventoryOpen = !inventoryOpen;

            if (inventoryOpen)
            {
                OpenInventory();
                DisableHandCollider();
            }
            else
            {
                EnableHandCollider();
                CloseInventory();
            }
        }
    }

    private void DisableHandCollider()
    {
        rightHandBox.enabled = false;
        leftHandBox.enabled = false;
    }

    private void EnableHandCollider()
    {
        rightHandBox.enabled = true;
        leftHandBox.enabled = true;
    }
    private void OpenInventory()
    {
        inventoryObject.SetActive(true);
    }

    private void CloseInventory()
    {
        inventoryObject.SetActive(false);
    }

    private void ShowCurrentSwordCount()
    {
        swordText.text = sword.ToString();
    }

    private void ShowCurrentArrowCount()
    {
        arrowText.text = arrow.ToString();
    }

    private void ShowCurrentBearCardCount()
    {
        bearCardText.text = bearCard.ToString();
    }
    private void ShowCurrentBossBearCardCount()
    {
        bossBearCardText.text = deerCard.ToString();
    }

    public void GameClear()
    {
        isGameClear = true;
        mainMenu.SetActive(false); 
        inventoryObject.SetActive(false); 
        Time.timeScale = 0f;
        gameClear.SetActive(true);
        StartCoroutine(FadeOut("S01_StartMenuScene", 3f));
    }
    public void BossSpawn()
    {
        if (bearCard >= 5 && deerCard >= 5)
        {
            bossPrefab.SetActive(true);
            Debug.Log("12312312");
        }
    }
    

    public void GameOver()
    {
        if (playerHp.currentHp <= 0f)
        {
            mainMenu.SetActive(false); // 게임메뉴 off
            inventoryObject.SetActive(false); // 인벤토리 off
            playerMovement.PlayerFallBack(); // 플레이어 넘어지는 함수 호출
            gameOver.SetActive(true);
            new WaitForSeconds(3f); 
            StartCoroutine(FadeOut("S01_StartMenuScene", 3f)); 
        }
    }

    public void ShowWeaponDurability()
    {
        weaponDurabilitySlider.value = _sword.breakCount / _sword.maxBreakCount;
    }

    private void PlusBearCnt()
    {
        bearCard++;
    }
    private void PlusDeerCnt()
    {
        deerCard++;
    }
}
