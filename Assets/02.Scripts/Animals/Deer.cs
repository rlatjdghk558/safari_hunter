using System.Collections;
using System.Collections.Generic;
using System.Threading;
using UnityEngine;

public class Deer : Animal
{
    [SerializeField] private int randomIdx;
    [SerializeField] private bool randomBehaviour = false;
    private float timer;
    //private Rigidbody rb;
   [SerializeField]private float force;
    [SerializeField] private float escapeSpeed;

    public delegate void VoidVoidDelegate();
    private VoidVoidDelegate dearDieDelegate;




    protected override void Awake()
    {
        base.Awake();
        attackRange = 3f;
        traceRange = 10f;
        //rb = GetComponent<Rigidbody>();
    }
    protected override void Start()
    {
        base.Start();
    }
    private void Update()
    {

        if (!isPatrolling)//순찰중
        {
            randomPatrolIdx = Random.Range(0, wayPoints.Length);
            isPatrolling = true;
        }
        if (!randomBehaviour)
        {
           // Debug.Log("randomBehaviour" + randomIdx);
            randomIdx = Random.Range(0, 5);//나중에 3 바꾸기
            randomBehaviour = true;
        }
        if(isDamaged == true)
        {
            
            timer += Time.deltaTime;
            if (timer > damagedTime)
            {
                isDamaged = false;
                timer = 0f;
               
            }
        }
    }
    protected override IEnumerator CheckState()
    {


        while (!isDie)
        {

            if (GetCurHp() <= 0f)//죽기
            {
                State = animalStates.Die;
                isDie = true;
            }
            if (!isDamaged)
            {
                if (randomIdx == 0)
                {
                    State = animalStates.Rest;
                }
                else if (randomIdx == 1)
                {
                    State = animalStates.Eat;
                }
                else
                {
                    State = animalStates.Patrol;
                }

                float dist = Vector3.Distance(transform.position, curWayPoints.transform.position);

                if (dist < 1f)//순찰지점 거의 다오면 다음 인덱스 뽑기
                {
                    isPatrolling = false;
                    //Debug.Log("pickNextIdx");
                    curWayPoints = wayPoints[randomPatrolIdx];
                }

            }
            else
            {
                State = animalStates.Escape;

            }
            yield return null;

        }
        yield return null;
    }
    protected override IEnumerator Action()
    {
        //yield return new WaitForSeconds(0.1f);//CheckState 보다 한템포 늦게 시작하기위한 시간지연
        //행동이 이상하면 이걸 없애고 한프레임 지연으로 바꿀것


        while (!isDie)
        {
            yield return null;

            switch (State)
            {
                case animalStates.Patrol:
                    //rb.velocity = Vector3.zero;
                   // Debug.Log("patrol");
                    randomBehaviour = true;
                    agent.isStopped = false;
                    anim.SetBool("isRun", false);
                    anim.SetBool("isWalk", true);
                    agent.destination = curWayPoints.transform.position;//Player위치 받는거 만들기
                    agent.speed = patrolSpeed;
                    break;

                case animalStates.Eat:
                   // Debug.Log("Eat");
                    randomBehaviour = true;
                    agent.isStopped = true;
                    anim.SetBool("isWalk", false);
                    anim.SetBool("isRun", false);
                    anim.SetTrigger("isEat");
                    break;

                case animalStates.Rest:
                    randomBehaviour = true;
                    agent.isStopped = true;
                   //Debug.Log("Rest");
                    anim.SetBool("isWalk", false);
                    anim.SetBool("isRun", false);
                    anim.SetTrigger("isRest");

                    break;

                case animalStates.Escape:
                   //Debug.Log("도망가기");
                    agent.isStopped = false;
                    anim.SetBool("isWalk", false);
                    anim.SetBool("isRun",true);
                    agent.speed = escapeSpeed;
                    break;

                case animalStates.Die:
                   // Debug.Log("die");
                    agent.isStopped = true;
                    anim.SetTrigger("isDie");
                    boxCollider.enabled = false;
                    Destroy(this.gameObject,5f);
                    break;
            }
        }
        yield return null;

    }
    public void EndRest()
    {
        //Invoke("EndBehaviour",10f);
        randomBehaviour = false;
       // Debug.Log("End");
    }
   
    public void EndEat()
    {
        //Invoke("EndBehaviour", 10f);
        randomBehaviour = false;
       // Debug.Log("End");
    }
    public void EndWalk()
    {
        //Invoke("EndBehaviour");
        randomBehaviour = false;
       // Debug.Log("End");
    }
    private void OnDestroy()
    {
        dearDieDelegate?.Invoke();
    }
    public void SetDelegate(VoidVoidDelegate _dearDieDelegate)
    {
        dearDieDelegate = _dearDieDelegate;
    }
    private void OnCollisionEnter(Collision collision)
    {
        if (collision.gameObject.CompareTag("WEAPON"))
        {
            curHp -= collision.gameObject.GetComponent<Weapon>().Damage();
            isDamaged = true;
        }


    }
}