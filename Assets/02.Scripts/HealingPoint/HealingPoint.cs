using System;
using System.Collections;
using System.Collections.Generic;
using System.Transactions;
using UnityEngine;

public class HealingPoint : MonoBehaviour
{
    public GameObject player = null;

    private PlayerHp playerHp;
    private float healingSpeed = 5f;
    private bool isAnimalStay = false;
    
    private void Awake()
    {
        playerHp = player.GetComponent<PlayerHp>();
    }

    private void OnTriggerStay(Collider other)
    {
        if (other.gameObject.CompareTag("PLAYER") && isAnimalStay == false)
        {
            playerHp.currentHp += Time.deltaTime * healingSpeed; //����
        }
        
        if(other.gameObject.CompareTag("ANIMAL"))
        {
            isAnimalStay = true;
        }
        else
        {
            isAnimalStay = false;
        }
    }

   
}
