using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class Larch : MonoBehaviour
{
    public bool isGround;
    public float dist = 10f;
    public float destroyTime;
    public Slider larchSlider;
    private Rigidbody rb;
    private RaycastHit hit;

    public float larchHp = 100f;
    private void Awake()
    {

        rb = GetComponent<Rigidbody>();
    }
    private void Update()
    {
        larchSlider.value = larchHp / 100f;
        if (Physics.Raycast(transform.position, -transform.up, out hit, dist))
        {
            if (hit.transform.gameObject.CompareTag("TERRAIN"))
            {
                isGround = true;
                rb.constraints = RigidbodyConstraints.FreezeAll;
            }
            else
            {
                isGround = false;
                destroyTime = Time.deltaTime;
            }
        }



    }

}
