using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class AttackCollider : MonoBehaviour
{
    [SerializeField] private float dmg;
    private void OnTriggerEnter(Collider _other)
    {
        if(_other.CompareTag("PLAYER"))
        {
            _other.GetComponent<PlayerHp>().currentHp -= dmg;
        }
    }
}
