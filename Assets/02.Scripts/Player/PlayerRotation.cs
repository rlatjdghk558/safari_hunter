using System;
using System.Collections;
using System.Collections.Generic;
using Oculus.Interaction;
using Unity.VisualScripting.Antlr3.Runtime;
using UnityEngine;

public class PlayerRotation : MonoBehaviour
{
    public Transform centerRot;

    private void Update()
    {
        float angleY = centerRot.transform.eulerAngles.y;
        transform.localRotation = Quaternion.Euler(0f, angleY, 0f);
        Debug.Log("몸통 :" + transform.eulerAngles.y);
        Debug.Log("카메라 :" +centerRot.transform.eulerAngles.y);
        centerRot.transform.forward = transform.forward;
        
    }
}
