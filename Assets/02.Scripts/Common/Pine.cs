using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class Pine : MonoBehaviour
{
    public bool isGround= false;
    public float dist = 10f;
    public float destroyTime;
    public Slider pineSlider;
    private RaycastHit hit;
    private Rigidbody rb;

    public float pineHp = 100f; 
    private void Awake()
    {
        rb = GetComponent<Rigidbody>();
    }
    private void Update()
    {
        pineSlider.value = pineHp / 100f;
        
        if (!isGround)
        {
            if (Physics.Raycast(transform.position, -transform.up, out hit, dist))
            {
                if (hit.transform.gameObject.CompareTag("TERRAIN"))
                {
                    isGround = true;
                    rb.constraints = RigidbodyConstraints.FreezeAll;
                }
                else
                {
                    isGround = false;
                    destroyTime = Time.deltaTime;
                }
            }
        }


    }

}
