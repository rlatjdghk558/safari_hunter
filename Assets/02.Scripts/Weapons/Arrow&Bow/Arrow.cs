using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Arrow : Weapon
{
    private Rigidbody rb = null;
    private BoxCollider boxCollider = null;

    public GameObject arrowEffect_1 = null;
    public GameObject arrowEffect_2 = null;

    public GameObject arrowSound;
    
    void Start()
    {
        dmg = 25.9f;
        rb = GetComponent<Rigidbody>();
        boxCollider = GetComponent<BoxCollider>();
        arrowEffect_1.SetActive(false);
        arrowEffect_2.SetActive(false);

    }

    private void Update()
    {
        if (rb.velocity.magnitude >= 0.2f)
        {
            transform.rotation = Quaternion.LookRotation(rb.velocity);//앞쪽을 계속 바라보게
        }
        if (OVRInput.GetUp(OVRInput.Button.SecondaryHandTrigger, OVRInput.Controller.Touch) && UIManager.arrow > 0)
        {
            arrowEffect_1.SetActive(true);
            arrowEffect_2.SetActive(true);
            arrowSound.SetActive(true);
        }
    }

    private void OnCollisionEnter(Collision col)
    {
        Debug.Log(col.transform.gameObject.name);
        if (!col.transform.gameObject.CompareTag("PLAYER"))
        {
            transform.SetParent(col.gameObject.transform);
            rb.isKinematic = true;
            boxCollider.enabled = false;
            arrowEffect_1.SetActive(false);
            arrowEffect_2.SetActive(false);
            arrowSound.SetActive(false);
            this.enabled = false;
            Destroy(gameObject, 5f); //충돌이 일어나면 파괴
        }
    }


}


