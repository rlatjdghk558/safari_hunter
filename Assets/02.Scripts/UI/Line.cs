using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class Line : MonoBehaviour
{
    public Transform buttonPos; // 버튼의 위치
    
    private LineRenderer line; // 라인
    private Transform textPos; // 텍슽트의 위치

    [Range(-0.2f, 0.2f)] public float linePosy;
    [Range(-0.2f, 0.2f)] public float linePosx;
       
    void Start()
    {
        // 텍스트의 위치를 textPos로 가져오기
        textPos = this.gameObject.GetComponent<Transform>();
        // 스크립트가 포함된 객체에 라인 렌더러라는 컴포넌트를 넣기.
        line = this.gameObject.AddComponent<LineRenderer>();        

        // 라인 색
        Material material = new Material(Shader.Find("Standard"));
        material.color = new Color(42, 105, 140, 0.5f);
        line.material = material;

        // 라인 꼭지점
        line.positionCount = 2;

        // 라인 굵기
        line.startWidth = 0.005f;
        line.endWidth = 0.005f;
        
        // 라인를 둥글게 하기
        line.numCornerVertices = 90;
        line.numCapVertices = 90;
    }

    void Update()
    {
        line.SetPosition(0, textPos.position);
        line.SetPosition(1, buttonPos.position + buttonPos.up * linePosy + buttonPos.right * linePosx);        
    }
}
