using JetBrains.Annotations;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using static UnityEditor.Progress;

public class RequireItemCount : MonoBehaviour

{
    public Slot[] slots;

    public Text countText;

    private int requireCount = 3;


    private void Update()
    {
        if (slots[0].GetComponent<Slot>().item != null)
        {
            Debug.Log(slots[0].GetComponent<Slot>().item.name);
            if (slots[0].GetComponent<Slot>().item.name == "Branch")
            {
                Debug.Log("1");
                countText.text = slots[0].GetComponent<Slot>().itemCount + "/" + requireCount;
            }
        }
    }
}
