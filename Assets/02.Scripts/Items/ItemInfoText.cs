using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class ItemInfoText : MonoBehaviour
{
    //?????? ??????? ????????? ????? ????? ????
    [SerializeField]
    private float range;//???? ???

    private bool canPick = false;//????????? true

    private RaycastHit hit;//?��? ???? ????

    [SerializeField]
    private LayerMask layerMask;

    [SerializeField]
    private Text infoText;

    [SerializeField]
    private Inventory theInventory;

    public Slot[] slots;
    public int branchCount;
    public int rockCount;

    private void Update()
    {
        TryGetItem();
        //Debug.Log("GetTIme" + getTime);
    }

    private void TryGetItem()
    {
        CheckItem();
        CanPickUp();
    }
    private void CheckItem()
    {
        if (Physics.Raycast(transform.position - new Vector3(0f, 1f, 0f), transform.forward, out hit, range, layerMask) ||
            Physics.Raycast(transform.position - new Vector3(0f, 1f, 0f), -transform.forward, out hit, range, layerMask) ||
            Physics.Raycast(transform.position - new Vector3(0f, 1f, 0f), transform.right, out hit, range, layerMask) ||
            Physics.Raycast(transform.position - new Vector3(0f, 1f, 0f), -transform.right, out hit, range, layerMask)) //????? ??????? ????
        {
            if (hit.transform.gameObject.CompareTag("BRANCH"))
            {
                ShowItemInfo();
                branchCount++;

            }
            else if (hit.transform.gameObject.CompareTag("ROCK"))
            {
                ShowItemInfo();
                rockCount++;
            }
        }
        else
        {
            DisappearInfo();
        }
    }
    private void ShowItemInfo()
    {
        canPick = true;
        infoText.gameObject.SetActive(true);
        //infoText.text = hit.transform.GetComponent<ItemPickUp>().item.itemName + "???" + "<color=yellow> " + "()?" + "</color>";

    }
    private void DisappearInfo()
    {
        canPick = false;
        infoText.gameObject.SetActive(false);
    }
    private void CanPickUp()
    {
        if (canPick == true)
        {
            if (hit.transform != null)
            {
                theInventory.AcquireItem(hit.transform.GetComponent<ItemPickUp>().item, 1);
                Destroy(hit.transform.gameObject);
                DisappearInfo();
            }
        }
    }
    public void CheckItemCount()
    {
        for (int i = 0; i < slots.Length; ++i)
        {
            {
                if (hit.transform.gameObject.GetComponent<ItemPickUp>().item.ToString() == "Branch")
                {
                    branchCount++;
                }
                else if (hit.transform.gameObject.GetComponent<ItemPickUp>().item.ToString() == "Rock")
                {
                    rockCount++;
                }

            }
        }
    }

}



