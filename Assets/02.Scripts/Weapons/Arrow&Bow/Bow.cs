using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Bow : Weapon
{
    public GameObject arrowPrefab = null;
    public Transform arrowPos = null;
    public GameObject arrowString = null;
    public float arrowSpeed; //Inspector 에서 정의
    public ParticleSystem bowEffect = null;

    private Rigidbody rb;
    private BoxCollider boxCollider;
    private GameObject arrow = null;
    private Vector3 arrowStringOriginPos;
    private float delayTime;

    private void Awake()
    {
        rb = GetComponent<Rigidbody>();
        boxCollider = GetComponent<BoxCollider>();
        //활 시위를 원위치 시키기 위한 원래 값 저장
        arrowStringOriginPos = arrowString.transform.localPosition;
        GetControllerTr();
    }



    private void Update()
    {
        bowEffect.Stop();
        if (OVRInput.GetDown(OVRInput.Button.SecondaryHandTrigger, OVRInput.Controller.Touch) && UIManager.arrow > 0) // 손에 잡았을때 조건 줘야함  
        {
            arrow = Instantiate(arrowPrefab, arrowString.transform.position, arrowString.transform.rotation);//왼쪽 손에 생성하고
            arrow.GetComponent<Rigidbody>().useGravity = false;
            arrow.GetComponent<BoxCollider>().enabled = false;
            arrowSpeed = 1f;//차징 초기화
            delayTime = 0;
            arrow.transform.SetParent(rightAnchor);

        }

        if (OVRInput.Get(OVRInput.Button.SecondaryHandTrigger, OVRInput.Controller.Touch) && arrowSpeed <= 30f && UIManager.arrow > 0 && arrow != null /*&& getString.stringIsGrabbed == true*/) // 누르고 있을때 차징
                                                                                                                                                                                                 //ArrowSpeed 최대치 30으로 고정
        {
            bowEffect.Play();
            Vector3 arrowDir = arrowPos.transform.position - arrowString.transform.position;
            Quaternion arrowRot = Quaternion.LookRotation(arrowDir.normalized);
            arrow.transform.rotation = arrowRot;


            arrow.transform.position = rightAnchor.position;
            arrowString.transform.position = rightAnchor.position;
            arrowSpeed = arrowDir.magnitude * 38;//차징 
        }

        else if (OVRInput.GetUp(OVRInput.Button.SecondaryHandTrigger, OVRInput.Controller.Touch) && arrow != null && UIManager.arrow > 0) //뗄때 날아감
        {
            arrow.transform.parent = null;
            //arrow가 날아가고나서 콜라이더가 켜지는 조건을 넣자
            arrow.GetComponent<Rigidbody>().useGravity = true;
            arrow.GetComponent<Rigidbody>().velocity = arrow.transform.forward * arrowSpeed;//차징된 스피드만큼 날아감
            arrow.GetComponent<BoxCollider>().enabled = true;
            arrowString.transform.localPosition = arrowStringOriginPos;//활시위 위치 원위치
            //bowEffect.Stop();
            UIManager.arrow--;
        }
  

        if (OVRInput.Get(OVRInput.Button.PrimaryHandTrigger, OVRInput.Controller.Touch))
        {
            transform.position = leftAnchor.transform.position;
            transform.rotation = leftAnchor.transform.rotation;
            rb.isKinematic = true;
            boxCollider.enabled = false;
        }
        else
        {
            rb.isKinematic = false;
            boxCollider.enabled = true;
        }

    }










}
