using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.AI;

public abstract class Animal : MonoBehaviour
{
    protected GameObject player;
    [Header("체력관련")]
    [SerializeField] protected float curHp;
    [SerializeField] protected float maxHp;
    [Header("범위 관련")]
    [SerializeField] protected float traceRange;
    [SerializeField] protected float attackRange;
    [Header("이동속도 관련")]
    [SerializeField] protected float traceSpeed;
    [SerializeField] protected float patrolSpeed;


    [SerializeField] protected bool isPatrolling = false;
    [SerializeField] protected int randomPatrolIdx;

    protected Animator anim;
    protected NavMeshAgent agent;

    [SerializeField] protected GameObject[] wayPoints;
    [SerializeField] protected GameObject curWayPoints;

    protected BoxCollider boxCollider;

    [SerializeField]public float damagedTime;
   [SerializeField] public bool isDamaged = false;
    protected bool isDie = false;

    public enum animalStates
    {
        Patrol, Trace, Attack, Howl, Hunt, Chase, Die,
        Rest, Eat, Wait, Escape
    };
    [SerializeField] protected animalStates State = animalStates.Patrol;

    protected virtual void Awake()
    {
        curHp = maxHp;
        anim = GetComponent<Animator>();
        agent = GetComponent<NavMeshAgent>();
        wayPoints = GameObject.FindGameObjectsWithTag("WayPoints");
        boxCollider = GetComponent<BoxCollider>();
        player = GameObject.FindGameObjectWithTag("PLAYER");
    }
    protected virtual void Start()
    {
        curWayPoints = wayPoints[Random.Range(0, wayPoints.Length)];
        StartCoroutine(CheckState());
        StartCoroutine(Action());
    }
    protected float GetDistance()
    {
        float distance = Vector3.Distance(transform.position, player.transform.position);
        return distance;
    }

    protected float GetCurHp()
    {
        return curHp;
    }

    protected float GetDetectionRange()
    {
        return traceRange;
    }

    protected float GetAttackRange()
    {
        return attackRange;
    }

    protected float GetHpRate()
    {
        return curHp / maxHp;
    }

    protected abstract IEnumerator CheckState();
    protected abstract IEnumerator Action();



}
