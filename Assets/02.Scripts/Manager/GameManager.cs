using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class GameManager : MonoBehaviour
{
    private GameObject player = null;
    private PlayerStamina playerStamina = null;
    private MeshRenderer larchMr;
    private MeshRenderer pineMr;
    private GameObject[] larches;
    private GameObject[] pines;
    private GameObject[] rocks;
    private MeshRenderer rockMr;

    public GameObject rock;
    public GameObject larch;
    public GameObject pine;

    public int larchCnt;
    public int pineCnt;
    public int rockCnt;
    public Transform treeGroup;
    private float meshOnTime;
    
    public LayerMask terrainLayer;

    public GameObject[] respawnGroups;
    public GameObject bearPrefab;
    private int bearCnt;
    public Transform animalGroup;

    public GameObject skillEffect;

    private EnemyManager enemyManager;
    private void Awake()
    {
        player = GameObject.FindGameObjectWithTag("PLAYER");
        playerStamina = player.GetComponent<PlayerStamina>();
        larchMr = larch.GetComponent<MeshRenderer>();
        rockMr = rock.GetComponent<MeshRenderer>();
        pineMr = pine.GetComponent<MeshRenderer>();
        enemyManager = GetComponentInChildren<EnemyManager>();
    }
    private void Start()
    {
        larchMr.enabled = false;
        rockMr.enabled = false;
        pineMr.enabled = false;
    }
    private void Update()
    {
        if (OVRInput.Get(OVRInput.Button.PrimaryIndexTrigger, OVRInput.Controller.Touch) || Input.GetKey(KeyCode.LeftShift))
        {
            SkillOn();
            if (playerStamina.playerStamina <= 1f)
            {
                SkillOff();
            }
        }
        else
        {
            SkillOff();
        }

        larches = GameObject.FindGameObjectsWithTag("LARCH");
        pines = GameObject.FindGameObjectsWithTag("PINE");
        rocks = GameObject.FindGameObjectsWithTag("ROCK");

        //환경 관련
        MakeTree();
        if (larches != null)
        {
            EnableTreeMesh();
        }

        MakeRock();
        if (rocks != null)
        {
            EnableRockMesh();
        }
        
        
    }


    private void MakeTree()
    {
        while (larchCnt <= 50)
        {

            Vector3 planeX = new Vector3(Random.Range(-150f, 70f), 1f, Random.Range(-40f, 200f));
            GameObject go = Instantiate(larch, planeX, Quaternion.identity);
            go.transform.SetParent(treeGroup);

            larchCnt++;
        }
        while (pineCnt <= 50)
        {

            Vector3 planeX = new Vector3(Random.Range(-150f, 70f), 1f, Random.Range(-40f, 200f));
            GameObject go = Instantiate(pine, planeX, Quaternion.identity);
            go.transform.SetParent(treeGroup);


            pineCnt++;
        }
    }
    private void EnableTreeMesh()
    {
        for (int i = 0; i < larches.Length; ++i)
        {
            if (larches[i].GetComponent<Larch>().isGround == true)
                larches[i].GetComponent<MeshRenderer>().enabled = true;
        }
        for (int i = 0; i < pines.Length; ++i)
        {
            if (pines[i].GetComponent<Pine>().isGround == true)
                pines[i].GetComponent<MeshRenderer>().enabled = true;
        }
    }
    private void MakeRock()
    {
        while (rockCnt <= 50)
        {

            Vector3 planeX = new Vector3(Random.Range(-150f, 70f), 1f, Random.Range(-40f, 200f));
            GameObject go = Instantiate(rock, planeX, Quaternion.identity);
            go.transform.SetParent(treeGroup);

            rockCnt++;
        }
    }
    private void EnableRockMesh()
    {
        for (int i = 0; i < rocks.Length; ++i)
        {
            if (rocks[i].GetComponent<Rock>().isGround == true)
            {
                rocks[i].GetComponent<MeshRenderer>().enabled = true;
            }
            else if (rocks[i].GetComponent<Rock>().isGround == true)
            {
                meshOnTime += Time.deltaTime;
                if (meshOnTime > 3f)
                {
                    rocks[i].GetComponent<MeshRenderer>().enabled = true;
                    meshOnTime = 0;
                }
            }
        }
    }
    
    private void SkillOn()//스킬 사용시
    {
        skillEffect.SetActive(true);

        Time.timeScale = 0.3f;
    }
    private void SkillOff()//스킬 꺼짐
    {
        skillEffect.SetActive(false);
        Time.timeScale = 1f;
    }
}