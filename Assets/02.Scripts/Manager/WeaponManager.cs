using System;
using System.Collections;
using System.Collections.Generic;
using Oculus.Interaction.PoseDetection;
using UnityEngine;
using UnityEngine.EventSystems;
using UnityEngine.SocialPlatforms.Impl;
using UnityEngine.UIElements;

public class WeaponManager : MonoBehaviour
{
    public GameObject leftHand;
    public GameObject rightHand;
    public GameObject sword;
    public GameObject bow;
    private int weaponIdx = 1;

    public GameObject punchPanel;
    public GameObject swordPanel;
    public GameObject arrowPanel;

    void Start()
    {
        TryGetHand();
    }

    void Update()
    {
        WeaponChanger();
    }

    public void WeaponChanger()
    {
        if (OVRInput.GetDown(OVRInput.Button.SecondaryThumbstick, OVRInput.Controller.Touch))
        {
            weaponIdx++;
            if (weaponIdx == 1)
            {
                TryGetHand();
            }
            else if (weaponIdx == 2)
            {
                TryGetSword();
            }
            else if (weaponIdx == 3)
            {
                TryGetBow();
                weaponIdx = 0;
            }
        }
    }

    private void TryGetHand()
    {
        leftHand.SetActive(true);
        rightHand.SetActive(true);
        sword.SetActive(false);
        bow.SetActive(false);
        punchPanel.SetActive(true);
        swordPanel.SetActive(false);
        arrowPanel.SetActive(false);
    }
    private void TryGetSword()
    {
        leftHand.SetActive(false);
        rightHand.SetActive(false);
        sword.SetActive(true);
        bow.SetActive(false);
        punchPanel.SetActive(false);
        swordPanel.SetActive(true);
        arrowPanel.SetActive(false);
    }
    private void TryGetBow()
    {
        leftHand.SetActive(false);
        rightHand.SetActive(false);
        sword.SetActive(false);
        bow.SetActive(true);
        punchPanel.SetActive(false);
        swordPanel.SetActive(false);
        arrowPanel.SetActive(true);
    }

}






