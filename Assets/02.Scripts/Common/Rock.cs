using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Rock : MonoBehaviour
{
    public bool isGround;
    public float dist = 1f;
    public float destroyTime;

    private RaycastHit hit;
    private Rigidbody rb;

    private void Awake()
    {
        rb = GetComponent<Rigidbody>();
    }

    private void Update()
    {
        if (Physics.Raycast(transform.position, -transform.up, out hit, dist))
        {
            if (hit.transform.gameObject.CompareTag("TERRAIN"))
            {
                isGround = true;
            }
            else if (GetComponent<MeshRenderer>().enabled == true)
            {
                rb.constraints = RigidbodyConstraints.FreezeAll;
            }
            else
            {
                isGround = false;
            }
        }
    }

}
