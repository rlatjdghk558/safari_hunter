using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;
using UnityEngine.UI;


public class SStartMenu : MonoBehaviour
{
    [SerializeField] private GameObject fadeCanvas;
    [SerializeField] private Image fadeImage;

    private HelpUI m_helpUI;
    private int helpCount = 1;

    private void Awake()
    {
        fadeCanvas.SetActive(true);
        m_helpUI = GetComponentInChildren<HelpUI>();
    }

    private void Start()
    {
        StartCoroutine(FadeIn());
    }

    void Update()
    {
        if(Input.GetKeyDown(KeyCode.V))
        {
            HelpMenu();
        }
    }

    public void StartGame()
    {
        StartCoroutine(FadeOut("S02_MainScene 1", 0.9f));
    }

    public void HelpMenu()
    {
        helpCount++;

        if(helpCount % 2 == 1)
        {
            m_helpUI.SetControllers(true);
        }
        else if(helpCount %2 == 0)
        {
            m_helpUI.SetControllers(false);
        }
    }
    
    public void ExitGame()
    {
        Application.Quit();
    }

    public void GoStartScene()
    {
        StartCoroutine(FadeOut( "S01_StartMenuScene", 0.9f));
    }

    private IEnumerator FadeIn()
    {
        float fadeCount = 1;
        while (fadeCount > 0.1f)
        {
            fadeCount -= 0.005f;
            yield return new WaitForSeconds(0.01f);
            fadeImage.color = new Color(0, 0, 0, fadeCount);
        }

        if (fadeCount < 0.11f)
            fadeCanvas.SetActive(false);
    }

    private IEnumerator FadeOut(string _sceneName, float _maxfadeCount)
    {
        fadeCanvas.SetActive(true);

        float fadeCount = 0;
        while (fadeCount < _maxfadeCount + 0.1f)
        {
            fadeCount += 0.01f;
            yield return new WaitForSeconds(0.01f);
            fadeImage.color = new Color(0, 0, 0, fadeCount);
        }
        if (fadeCount >= _maxfadeCount) 
            SceneManager.LoadScene(_sceneName);
    }
    
}