using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[System.Serializable]
public struct PlayerSfx
{
    public AudioClip[] background;
}
public class BackSound : MonoBehaviour
{
    public AudioSource backGround;

    void Start()
    {
        backGround = GetComponent<AudioSource>();
    }

    // Update is called once per frame
    void Update()
    {
        backGround.Play();
    }
}
