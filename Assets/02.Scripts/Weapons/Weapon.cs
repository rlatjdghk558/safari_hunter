using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Weapon : MonoBehaviour
{
    protected float dmg;

    protected Transform rightAnchor = null;
    protected Transform leftAnchor = null;

    protected void GetControllerTr()
    {
        rightAnchor = GameObject.FindGameObjectWithTag("RIGHTHAND").GetComponent<Transform>();
        leftAnchor = GameObject.FindGameObjectWithTag("LEFTHAND").GetComponent<Transform>();
    }
    public float Damage()
    {
        return dmg;
    }


}
