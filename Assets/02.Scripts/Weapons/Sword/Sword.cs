using System.Collections;
using System.Collections.Generic;
using System.Transactions;
using UnityEngine;

public class Sword : Weapon
{
    public Material branchMat;
    public Material rockMat;
    public Material pineMat;
    public GameObject branch;
    public GameObject rock;
    public GameObject pine;
    
    public RopeAction ropeAction;
    public GameManager gameManager;

    public float maxBreakCount = 50;
    public float breakCount;

    private void Awake()
    {
        dmg = 35f;
        GetControllerTr();
        breakCount = maxBreakCount;

    }

    private void Update()
    {
        if (OVRInput.Get(OVRInput.Button.SecondaryHandTrigger, OVRInput.Controller.Touch) && UIManager.sword > 0)  //bowString.isGrabbed == true)//������
        {
            transform.position = rightAnchor.position;
            transform.rotation = rightAnchor.rotation * Quaternion.Euler(0f, 270f, -20f);
            transform.SetParent(rightAnchor);
        }
        else
        {
            transform.parent = null;
        }

            //Debug.Log(breakCount);
        if (breakCount == 0)
        {
            Destroy(gameObject);
            UIManager.sword--;
            breakCount = maxBreakCount;
        }
    }
    
    private void OnCollisionExit(Collision other)
    {
        if (other.gameObject.CompareTag("LARCH"))
        {
            MeshCut.Cut(other.gameObject, transform.position, Vector3.up, branchMat);
            ropeAction.EndShoot();
            Instantiate(branch, other.transform.position, other.transform.rotation);
            gameManager.larchCnt--;
            breakCount--;
        }
        else if (other.gameObject.CompareTag("PINE"))

        {
            MeshCut.Cut(other.gameObject, transform.position, Vector3.up, branchMat);
            ropeAction.EndShoot();
            Instantiate(branch, other.transform.position, other.transform.rotation);
            gameManager.pineCnt--;
            breakCount--;
        }
       
    }
    

}

