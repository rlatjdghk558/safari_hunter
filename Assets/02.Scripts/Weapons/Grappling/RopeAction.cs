using System.Collections;
using System.Collections.Generic;
using Unity.VisualScripting;
using UnityEngine;

public class RopeAction : Weapon
{
    //1. 레이 캐스트
    //2. 라인랜더러
    //3. 스프링 조인트 연결

    private Transform hookPos = null;
    public Transform playerTr = null;
    public LayerMask treeLayer;

    public float hookDistance = 50f;
    //public Transform gunTip;

    private RaycastHit hit;
    private LineRenderer line;
    private bool isGrappling = false;
    private SpringJoint sj;
    private GameObject player = null;
    private Rigidbody playerRb = null;


    private void Start()
    {
        GetControllerTr();
        player = GameObject.FindGameObjectWithTag("PLAYER");
        line = GetComponent<LineRenderer>();
        playerRb = player.GetComponent<Rigidbody>();
        sj = player.GetComponent<SpringJoint>();

    }
    private void Update()
    {
        if (OVRInput.GetDown(OVRInput.Button.SecondaryIndexTrigger, OVRInput.Controller.Touch))
        {
            RopeShoot();
        }

        else if (OVRInput.Get(OVRInput.Button.Two, OVRInput.Controller.Touch))
        {
            if (sj != null)
            {
                float dist = Vector3.Distance(playerTr.position, hookPos.position);
                playerTr.position = Vector3.Lerp(playerTr.position, hookPos.position, Time.deltaTime * 0.65f);
                if (dist <= 10f && hookPos != null)
                {
                    EndShoot();
                }
                playerRb.useGravity = false;

            }
        }

        else if (OVRInput.GetUp(OVRInput.Button.SecondaryIndexTrigger, OVRInput.Controller.Touch) && sj != null)
        {
            Debug.Log("up");
            EndShoot();
            playerRb.AddForce(Vector3.forward * playerRb.velocity.z * 500f);
        }

        if (hookPos != null)
        {
            DrawRope();
        }
    }

    void RopeShoot()
    {
        if (Physics.Raycast(rightAnchor.position, rightAnchor.forward, out hit, hookDistance, treeLayer))
        //if(Physics.Raycast(playerTr.position, playerTr.forward, out hit, 100f, treeLayer))
        {
            isGrappling = true;
            line.positionCount = 2;
            hookPos = hit.transform.gameObject.GetComponentInChildren<HookPoint>().transform;
            //스크립트를 만들어서 걔를 들고오면 됨
            line.SetPosition(0, rightAnchor.position);
            //  line.SetPosition(0, playerTr.position);
            line.SetPosition(1, hookPos.position);
            sj = playerTr.gameObject.AddComponent<SpringJoint>();
            playerRb.freezeRotation = true;
            sj.autoConfigureConnectedAnchor = false; //앵커의 위치를 자동으로 계산해주는데 이게 활성화 돼있으면 안됨, 우리가 직접 설정할거라서
                                                     //sj.connectedAnchor = spot;
            sj.connectedAnchor = hookPos.position;

            float dist = Vector3.Distance(playerTr.position, hookPos.position);
            sj.maxDistance = dist * 0.75f;
            sj.minDistance = dist * 0.5f;
            sj.spring = 100f;//강도
            sj.damper = 100f;//줄어드는 힘
            sj.massScale = 5f;
        }
    }
    public void EndShoot()
    {
        isGrappling = false;
        line.positionCount = 0;
        Destroy(sj);
    }

    void DrawRope()
    {
        if (isGrappling)
        {
            line.SetPosition(0, rightAnchor.position);
            playerTr.position = Vector3.Lerp(playerTr.position, hookPos.position, 0.001f);
        }
    }

}