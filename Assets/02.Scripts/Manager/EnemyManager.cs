using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class EnemyManager : MonoBehaviour
{
    [SerializeField] private GameObject bearPrefab;
    [SerializeField] private GameObject deerPrefab;
    private float bearCnt;
     private float deerCnt;
    [SerializeField] private Transform[] wayPoints;
    private BearGroup bearGroup;
    private DeerGroup deerGroup;
    private void Awake()
    {
        bearGroup = GetComponentInChildren<BearGroup>();
        deerGroup = GetComponentInChildren<DeerGroup>();
    }
    private void Start()
    {
        SpawnBear();
        SpawnDeer();
    }

    private void SpawnBear()
    {
            while (bearCnt < 20)
            {
                GameObject go = Instantiate(bearPrefab, wayPoints[Random.Range(0, wayPoints.Length)].transform.position, wayPoints[Random.Range(0, wayPoints.Length)].transform.rotation);
                go.transform.SetParent(bearGroup.GetTr());
                bearCnt++;
            }
    }
    private void SpawnDeer()
    {
        while (deerCnt < 20)
            {
                GameObject go = Instantiate(deerPrefab, wayPoints[Random.Range(0, wayPoints.Length)].transform.position, wayPoints[Random.Range(0, wayPoints.Length)].transform.rotation);
                go.transform.SetParent(deerGroup.GetTr());
                deerCnt++;
            }
    }
}
